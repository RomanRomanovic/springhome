package cz.vsb.chmelar.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import connector.ApixuConnector;
import cz.vsb.chmelar.City;
import dto.Location;
import dto.WeatherDto;
import service.WeatherService;

@RestController
public class WeatherController {
	@RequestMapping ("/weather")	
	public String getWeather() {
		return "Počasí pro všechna města";
		}
	
	@RequestMapping ("/weather/{city}")	
	public WeatherDto getWeatherForCity(@PathVariable String city) {
		City cityEnum = City.valueOf(city.toUpperCase());
		WeatherService weatherService = new WeatherService();
		return weatherService.getWeatherForCity(cityEnum);
		
		/*City cityEnum = City.valueOf(city.toUpperCase());
		ApixuConnector connector = new ApixuConnector();
		String cty = connector.getWeatherForCity(cityEnum);
		WeatherDto weatherDto = new WeatherDto();		
		weatherDto.setLocation(cty);
		//City.valueOf(city.toUpperCase());
		
		//String location = connector.getWeatherForCity(City.valueOf(city.toUpperCase()));
				
		return weatherDto;*/
		}

}
