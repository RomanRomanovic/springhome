package dto;

public class WeatherDto {
	private String location;
	private String timestamp;
	private double temp_celsius;
	private float rel_humidity;
	private double windSpeed_mps;	
	private String windDirection;	
	private String windDescription;		
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public double getTemp_celsius() {
		return temp_celsius;
	}
	public void setTemp_celsius(double temp_celsius) {
		this.temp_celsius = temp_celsius;
	}
	public float getRel_humidity() {
		return rel_humidity;
	}
	public void setRel_humidity(float rel_humidity) {
		this.rel_humidity = rel_humidity;
	}
	public double getWindSpeed_mps() {
		return windSpeed_mps;
	}
	public void setWindSpeed_mps(double windSpeed_mps) {
		this.windSpeed_mps = windSpeed_mps;
	}
	public String getWindDirection() {
		return windDirection;
	}
	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}
	public String getWindDescription() {
		return windDescription;
	}
	public void setWindDescription(String windDescription) {
		this.windDescription = windDescription;
	}	
}
