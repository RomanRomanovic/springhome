package dto;

public class Current {

		 private String last_updated;
		 private float temp_c;
		 private float wind_kph;
		 private String wind_dir;
		 private float humidity;
		Condition condition;


		 // Getter Methods 

		public String getLast_updated() {
		  return last_updated;
		 }

		public float getTemp_c() {
		  return temp_c;
		 }

		public float getWind_kph() {
		  return wind_kph;
		 }

		 public String getWind_dir() {
		  return wind_dir;
		 }

		 public float getHumidity() {
		  return humidity;
		 }

		 // Setter Methods 
		
		 public void setLast_updated(String last_updated) {
		  this.last_updated = last_updated;
		 }

		 public void setTemp_c(float temp_c) {
		  this.temp_c = temp_c;
		 }

		 public void setWind_kph(float wind_kph) {
		  this.wind_kph = wind_kph;
		 }
		
		 public void setWind_dir(String wind_dir) {
		  this.wind_dir = wind_dir;
		 }
		
		 public void setHumidity(float humidity) {
		  this.humidity = humidity;
		 }
	
		
}
