package service;

import org.springframework.stereotype.Service;

import connector.ApixuConnector;
import cz.vsb.chmelar.City;
import dto.ApixuDto;
import dto.WeatherDto;

@Service
public class WeatherService {
	public WeatherDto getWeatherForCity(City cityEnum) {
		//City cityEnum = City.valueOf(city.toUpperCase());
		ApixuConnector connector = new ApixuConnector();
		ApixuDto apixuDto = connector.getWeatherForCity(cityEnum);
		return transformDto(apixuDto);		
	}
	private WeatherDto transformDto(ApixuDto apixuDto) {
		WeatherDto wdto = new WeatherDto();
		wdto.setLocation(apixuDto.getLocation().getName());
		wdto.setRel_humidity(apixuDto.getCurrent().getHumidity());
		wdto.setTemp_celsius(apixuDto.getCurrent().getTemp_c());
		wdto.setTimestamp(apixuDto.getCurrent().getLast_updated());
		//wdto.setWindDescription(apixuDto.get); - mam tam nejaky bordel neco mi tam chybi
		wdto.setWindDirection(apixuDto.getCurrent().getWind_dir());
		wdto.setWindSpeed_mps(apixuDto.getCurrent().getWind_kph());
		
		return wdto;		
	}
}
