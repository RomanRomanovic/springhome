package connector;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import cz.vsb.chmelar.City;
import dto.ApixuDto;

public class ApixuConnector {
	private static String baseUrl = "http://api.apixu.com/";
	private static String urlParams = "v1/current.json?key=";
	private static String APIkey = "3b81f78ed2a64cdd82a151330192904";
	private static String url = baseUrl + urlParams + APIkey + "&q=";
	// http://api.apixu.com/v1/current.json?key=3b81f78ed2a64cdd82a151330192904&q=Paris
	

	public ApixuDto getWeatherForCity(City city) {
		RestTemplate template = new RestTemplate();
		URI uri = null;
		
		try {
			uri = new URI(url + city.toString());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		ResponseEntity<ApixuDto> response = template.getForEntity(uri, ApixuDto.class);
		return response.getBody();  //
		}
	
}
